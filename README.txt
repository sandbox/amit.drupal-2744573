***********
* README: *
***********

DESCRIPTION:
------------
This module provides an show password and genrate random strong password field.


INSTALLATION:
-------------
1. Place the entire passwordwidgets directory into Drupal sites/all/modules/
   directory.

2. Enable the passwordwidgets module by navigating to:

     administer > modules


Features:
---------
  * Password toggle field and display in all password field
  * Three Type Password toggle setting fields are display
      o Show passwrd click on image
      o Click on field show and hide field display
      o Show password check box display
  * Generate strong password in registration form
      o Choose password Length
      o Choode password Pattern


Note:
-----
Install module and go to admin/config/user-interface/passwordwidgets page.
Go to admin/config/people/accounts and uncheck "Require e-mail
verification when a visitor creates an account." field for
use Generate strong password feature.


Author:
-------
Amit Kumar
amit.drupal@drupal.org
amit.drupal@gmail.com
