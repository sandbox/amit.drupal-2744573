(function($) {
	Drupal.behaviors.adminMenuLivePreview = {
	  attach: function (context, settings) {
  	  /*
	     * Show hide configuration fields in Backends
	     */ 
	    var checkedValue = $('#edit-passwordwidgets-generate-strong-password:checked').val();
	    if(checkedValue == 1) {
	      $('.form-item-password-range').show();
	      $('.form-item-password-type').show();
	    } else {
	      $('.form-item-password-range').hide();
	      $('.form-item-password-type').hide();
	    }
	    $("#edit-passwordwidgets-generate-strong-password").click(function() { 
	      var checkedValue = $('#edit-passwordwidgets-generate-strong-password:checked').val();
	      if(checkedValue == 1) {
	        $('.form-item-password-range').slideDown();
	        $('.form-item-password-type').slideDown();
	      } else {
	        $('.form-item-password-type').slideUp();
	        $('.form-item-password-range').slideUp();
	      }
	    });

	    var passSetting = Drupal.settings.passwordwidgets.password_config;
		  var strongSetting = Drupal.settings.passwordwidgets.strong_password;
	    var passwordRange = Drupal.settings.passwordwidgets.passwordwidgets_password_range;
	    var passwordType = Drupal.settings.passwordwidgets.passwordwidgets_password_type;

			if(passwordType == undefined) { passwordType = 1; }

	    if (passSetting != undefined) {
	      if(passSetting == 1) {
	        $('input:password').wrap("<div class='password-main-wrapper'></div>");
	        $('<div class="image-toggle"><div class="edit-pass-images showHide" ></div></div>').insertAfter('input:password');
	      } else if (passSetting == 2) {
	        $('<div class="checkbox-toggle"><input type="checkbox"  class="pass-checkbox showHide"/><label for="showHide" id="showHideLabel">Show Password</label></div>').insertAfter('input:password');
	        $('input:password').addClass("parent-check-box-toggle");
	      } else if (passSetting == 3) {
	        $('input:password').wrap("<div class='password-main-wrapper'></div>");
	        $('<div class="texts-toggle"><label class="text showHide" >Show </label></div>').insertAfter('input:password');
	        $('.texts-toggle').hide();
	        $('input:password').css("padding-left","5px");
	        $('input:password').click(function() {
	        	$(this).css("padding-left","50px");
	          $(this).next().slideDown();
	        });
	      }

	      $(".showHide").click(function() {
	        if ($(this).parent().prev().attr("type") == "password") {
	          $(this).parent().prev().attr("type", "text");
	          $(this).addClass("pass-shown");
	          $(this).removeClass("pass-hidden");
	          $(".text.pass-shown").html("Hide");         
	        } else {
	          $(this).parent().prev().attr("type", "password");
	          $(this).addClass("pass-hidden");
	          $(this).removeClass("pass-shown");
	          $(".text.pass-hidden").html("Show");
	        }
	      });
	    }

		  if(strongSetting == 1) {
	      function randomString() {
	        var chars = calculatePasswordType(passwordType);
	        var string_length = calculatePasswordRange(passwordRange);
	        var randomstring = '';
	        for (var i=0; i<string_length; i++) {
	          var rnum = Math.floor(Math.random() * chars.length);
	          randomstring += chars.substring(rnum,rnum+1);
	        }
	        
        return randomstring;
	      }

	      if($('.form-item-pass').length) {
	        $('.form-item-pass').append('<div class="strong-password"><a class="generate-password">Generate Password</a><a class="use-password">Use Password</a><div class="generated-password"></div></div>');
	        
	        $(".generate-password").click(function() {
	          var randomstring = randomString();
	          $('.generated-password').html(randomstring);
	          $('.use-password').css("visibility","visible");
	        });

	        $(".use-password").click(function() {
	          var selectPassword = $('.generated-password').html();
	          if(selectPassword.length > 0) {
	            $('input:password').val(selectPassword);
            }
	        });

	      }
	    }

      /*
	     * Function for Generate random password  
	     */
	    function calculatePasswordType(passwordType) {
	      if(passwordType == 1) {
	        return '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz~!@#$%&';
	      } else if(passwordType == 2) {
	        return 'ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
	      } else if(passwordType == 3) {
	        return '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
	      } else if(passwordType == 4) {
	        return '0123456789~!@#$%&';
	      } else if(passwordType == 5) {
	        return 'ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz~!@#$%&';
	      } else {
	        return '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
	      }
	    }

	    /*
	     * Function for get password range 
	     */
	    function calculatePasswordRange(passwordRange) {
	      if(passwordRange == 1) {
	        return '5';
	      } else if(passwordRange == 2) {
	        return '8';
	      } else if(passwordRange == 3) {
	        return '10';    
	      } else if(passwordRange == 4) {
	        return '12';    
	      } else if(passwordRange == 5) {
	        return '15';    
	      } else if(passwordRange == 6) {
	        return '18';   
	      } else if(passwordRange == 7) {
	        return '20';    
	      } else {
	        return '10';
	      }
	    }  

    }
	};
})(jQuery);
